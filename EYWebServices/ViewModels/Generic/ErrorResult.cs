﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace EYWebServices.ViewModels.Generic
{
    public class ErrorResult : IHttpActionResult
    {
        private Response _error;
        private HttpRequestMessage _request;

        public ErrorResult(Response error, HttpRequestMessage request)
        {
            _error = error;
            _request = request;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            var response = new HttpResponseMessage(_error.StatusCode)
            {
                Content = new ObjectContent<Response>(_error, new JsonMediaTypeFormatter()),
                RequestMessage = _request
            };

            return Task.FromResult(response);
        }
    }
}