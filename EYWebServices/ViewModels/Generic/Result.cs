﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EYWebServices.ViewModels.Generic
{
    public enum Result
    {
        Success,
        Failed,
        NotFound,
        Unauthorized,
        InvalidUsername,
        InvalidCode
    }
}