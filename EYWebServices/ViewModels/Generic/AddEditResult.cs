﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EYWebServices.ViewModels.Generic
{
    public class AddEditResult
    {
        [JsonProperty("result")]
        [JsonConverter(typeof(StringEnumConverter))]
        public Result Result { get; set; }

        [JsonProperty("id")]
        public int? Id { get; set; }
    }
}