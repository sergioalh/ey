﻿using EYWebServices.Models;
using EYWebServices.ViewModels.Articulo;
using EYWebServices.ViewModels.Generic;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;

namespace EYWebServices.Logic.ArticuloRepository
{
    public class ArticuloRepository:IArticuloRepository
    {
        private readonly eyEntities _context = new eyEntities();

        public async Task<AddEditResult> AddEditArticulo(ArticuloVM model)
        {
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    var Articulo = await _context.articulo.FindAsync(model.Id);
                    if (Articulo == null)
                    {
                       
                        _context.articulo.Add(Articulo);
                    }

                    Articulo.nombre = model.Nombre;
                    Articulo.descripcion = model.Descripcion;
                    Articulo.cantidad = model.Cantidad;
                    Articulo.codigo = model.Codigo;



                    await _context.SaveChangesAsync();
                    transaction.Complete();

                    return new AddEditResult
                    {
                        Result = Result.Success,
                        Id = Articulo.id
                    };
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new AddEditResult
                {
                    Result = Result.Failed
                };
            }
        }

        public async Task<Result> DeleteArticulo(int id)
        {
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    var Articulo = await _context.articulo.FirstOrDefaultAsync(x => x.id == id);

                    if (Articulo == null) return Result.Failed;

                  
                    await _context.SaveChangesAsync();
                    transaction.Complete();

                    return Result.Success;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return Result.Failed;
            }
        }

        public async Task<ArticuloVM> GetArticuloById(int id)
        {
            try
            {
                var ArticuloEntity = await _context.articulo.FirstOrDefaultAsync(x => x.id == id);

                if (ArticuloEntity == null) return null;

                var Articulo = new ArticuloVM
                {
                    Id = ArticuloEntity.id,
                    Nombre = ArticuloEntity.nombre,
                    Codigo = ArticuloEntity.codigo,
                    Cantidad = ArticuloEntity.cantidad,
                    Descripcion = ArticuloEntity.descripcion
                };


                return Articulo;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<List<ArticuloVM>> GetArticulo()
        {
            try
            {
                var query = _context.articulo.AsQueryable();



                var ArticulosList = query.AsEnumerable().Select(x =>
                    new ArticuloVM
                    {
                      Descripcion = x.descripcion,
                      Cantidad = x.cantidad,
                      Codigo = x.codigo,
                      Id = x.id,
                      Nombre = x.nombre

                    }).ToList();

                return ArticulosList;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}