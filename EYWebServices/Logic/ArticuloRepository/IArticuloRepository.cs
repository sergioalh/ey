﻿using EYWebServices.ViewModels.Articulo;
using EYWebServices.ViewModels.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace EYWebServices.Logic.ArticuloRepository
{
    public interface IArticuloRepository
    {
        Task<List<ArticuloVM>> GetArticulo();
        Task<ArticuloVM> GetArticuloById(int id);
        Task<AddEditResult> AddEditArticulo(ArticuloVM model);
        Task<Result> DeleteArticulo(int id);
    }
}