﻿using EYWebServices.Logic.ArticuloRepository;
using EYWebServices.ViewModels.Articulo;
using EYWebServices.ViewModels.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace EYWebServices.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/articulos")]
    public class ArticuloController : ApiController
    {
        private readonly IArticuloRepository _repository;
        protected Response response = new Response();

        public ArticuloController(IArticuloRepository repository)
        {
            _repository = repository;
        }


        [HttpPost]
        [Route("")]
        public async Task<IHttpActionResult> AddArticulo([FromBody] ArticuloVM model)
        {
            try
            {
                var result = await _repository.AddEditArticulo(model);
                if (result.Result == Result.Failed)
                {
                    response.StatusCode = HttpStatusCode.BadRequest;
                    response.Status = "Bad Request";
                    response.Message = "Could not perform the operation";
                    response.Result = result;
                    return new ErrorResult(response, Request);
                }

                response.StatusCode = HttpStatusCode.Created;
                response.Status = "Created";
                response.Result = result;

                return Ok(response);
            }
            catch (Exception e)
            {
                response.StatusCode = HttpStatusCode.BadRequest;
                response.Status = "Bad Request";
                response.Message = e.Message;
                return new ErrorResult(response, Request);
            }
        }

        [HttpPut]
        [Route("{id}")]
        public async Task<IHttpActionResult> EditArticulo(int id, [FromBody] ArticuloVM model)
        {
            try
            {
                if (id != model.Id)
                {
                    response.StatusCode = HttpStatusCode.BadRequest;
                    response.Status = "Bad Request";
                    response.Message = "The URI id and model serviceId does not match";
                    return new ErrorResult(response, Request);
                }

                var result = await _repository.AddEditArticulo(model);
                if (result.Result == Result.Failed)
                {
                    response.StatusCode = HttpStatusCode.BadRequest;
                    response.Status = "Bad Request";
                    response.Message = "Could not perform the operation";
                    response.Result = result;
                    return new ErrorResult(response, Request);
                }

                response.StatusCode = HttpStatusCode.OK;
                response.Status = "OK";
                response.Result = result;

                return Ok(response);
            }
            catch (Exception e)
            {
                response.StatusCode = HttpStatusCode.BadRequest;
                response.Status = "Bad Request";
                response.Message = e.Message;
                return new ErrorResult(response, Request);
            }
        }


        [HttpGet]
        [Route("")]
        public async Task<IHttpActionResult> GetArticulos()
        {
            try
            {
                response.StatusCode = HttpStatusCode.OK;
                response.Status = "OK";
                response.Result = await _repository.GetArticulo();

                return Ok(response);
            }
            catch (Exception e)
            {
                response.StatusCode = HttpStatusCode.BadRequest;
                response.Status = "Bad Request";
                response.Message = e.Message;

                return new ErrorResult(response, Request);
            }
        }

        [HttpGet]
        [Route("{id}")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> GetArticuloById(int id)
        {
            try
            {
                var service = await _repository.GetArticuloById(id);
                if (service == null)
                {
                    response.StatusCode = HttpStatusCode.NotFound;
                    response.Status = "Not Found";
                    response.Message = "Service not found";
                    return new ErrorResult(response, Request);
                }

                response.StatusCode = HttpStatusCode.OK;
                response.Status = "OK";
                response.Result = service;

                return Ok(response);
            }
            catch (Exception e)
            {
                response.StatusCode = HttpStatusCode.BadRequest;
                response.Status = "Bad Request";
                response.Message = e.Message;

                return new ErrorResult(response, Request);
            }
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task<IHttpActionResult> DeleteArticulo(int id)
        {
            try
            {
                var result = await _repository.DeleteArticulo(id);
                if (result == Result.Failed)
                {
                    response.StatusCode = HttpStatusCode.BadRequest;
                    response.Status = "Bad Request";
                    response.Message = "Could not perform the operation";
                    return new ErrorResult(response, Request);
                }

                response.StatusCode = HttpStatusCode.OK;
                response.Status = "OK";

                return Ok(response);
            }
            catch (Exception e)
            {
                response.StatusCode = HttpStatusCode.BadRequest;
                response.Status = "Bad Request";
                response.Message = e.Message;
                return new ErrorResult(response, Request);
            }
        }
    }
}